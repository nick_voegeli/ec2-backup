INSTANCE_ID = $(shell curl -s http://169.254.169.254/latest/meta-data/instance-id)
TYPE = adhoc
KEEP = 1
TS = ts '[%Y-%m-%d %H:%M:%S]'
OWNER = 738597539057

aws-backup: $(AWS)
	./aws-backup.sh $(OWNER) $(INSTANCE_ID) $(TYPE) $(KEEP) 2>&1 | $(TS) >> log/aws-backup.log