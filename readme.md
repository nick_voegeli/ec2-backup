# EC2 EBS Snapshot Tools


## **About**

This is an automated solution to scheduling backups of EC2 instances using EBS volumes.  It is typically triggered via cron.  The files in this directory are simply copied onto the server that should be backed up, then the task is scheduled via cron.


## **Dependencies**

* `make` - installed by default in EC2's Ubuntu 16.04 images.  If you need to install, use `sudo apt-get install build-essential`
* [`moreutils`](https://rentes.github.io/unix/utilities/2015/07/27/moreutils-package/) - `sudo apt-get install moreutils`
* Python [2.65+](https://askubuntu.com/questions/101591/how-do-i-install-the-latest-python-2-7-x-or-3-x-on-ubuntu) or [3.3+](http://ubuntuhandbook.org/index.php/2017/07/install-python-3-6-1-in-ubuntu-16-04-lts/) (3.5 comes installed in the AWS Ubuntu 16.04 AMI)
* `pip` - `sudo apt-get install python3-pip`
* [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/installing.html) - `pip install awscli --upgrade --user`

## **Installation**

1. Install all dependencies
1. Create an IAM role with full EC2 privileges and CLI access
1. Create `~/.aws/config` containing the region for this instance:

		[default]
		region = us-east-1

1. Create `~/.aws/credentials` containing the IAM information for your new role:

		[default]
		aws_access_key_id = ALIFJTM5U3M3CUHUURIQ
		aws_secret_access_key = L/eWfxDkmW/ct6dkWg1i0wWBh1w7cQs0TEC2Ru

1. Copy the files from this project to `~/ec2-backup` on your instance: `scp -r ec2-backup/ {your server}:~`
1. **Important**: modify the `aws-backup.sh` script on the server to contain the full path 
1. Manually test image creation: `make aws-backup` and verify the presence of the new image in the EC2 GUI
1. Schedule the cron task - see below

## **Scheduling**

Add the following to the crontab.  Substitute the actual path to the `aws` binary when setting the `PATH` environment variable.

```
#=========================================================================================
# AMI images & EBS snapshots
# https://bitbucket.org/nick_voegeli/ec2-backup/

# cron tasks don't run in the shell, so the path in .bashrc is not available unless specified
PATH=/home/ubuntu/bin:/home/ubuntu/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin

# Daily Backup
5 1 * * * cd /home/ubuntu/ec2-backup && make aws-backup TYPE=daily KEEP=6 > /dev/null 2>&1

# Weekly Backup
0 12 * * 0 cd /home/ubuntu/ec2-backup && make aws-backup TYPE=weekly KEEP=3 > /dev/null 2>&1

# Monthly Backup
0 2 1 * * cd /home/ubuntu/ec2-backup && make aws-backup TYPE=monthly KEEP=8 > /dev/null 2>&1
```

## **Troubleshooting**

If you encounter issues with backups not running successfully, try the following:

1. First, save a snapshot of the environment in which cron runs.  Add the following cron task:

		* * * * *   /usr/bin/env > /home/ubuntu/cron-env

	After the file is saved, remove the cron task so it doesn't continue to run every minute.

1. Create a shell script to run scripts from within the cron environment.  We'll use `/bin/run-as-cron`:

		#!/bin/bash
		/usr/bin/env -i $(cat /home/ubuntu/cron-env) "$@"

1. Create a test script to run the cron command you want to troubleshoot. For this example, we'll create `/home/ubuntu/ec2-backup/test.sh`:

		PATH=/home/ubuntu/bin:/home/ubuntu/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
		make aws-backup TYPE=daily KEEP=6

1. Run your test script: `run-as-cron /home/ubuntu/ec2-backup/test.sh`