#!/bin/bash
#
# Original: https://github.com/rgrignon/AWS-AMI-Backup/blob/master/aws_ami_backup.sh

CURDATE=$(date +%Y-%m-%d)
OWNERS=$1
INSTANCEID=$2
TYPE=$3
NUMKEEP=$4

set -e

#########################

i=$INSTANCEID

echo "Preparing to backup instance: $i"

NAME=main
CURTIME=$(date +%s)         #Epoch Timestamp
IMGNAME="${NAME}-${TYPE}-${CURDATE}_${CURTIME}"

#create the images
echo "Backup Start: $IMGNAME ($i)"

#create the image
AMIID=$(aws ec2 create-image --no-reboot --instance-id $i --name ${IMGNAME} --description "${CURTIME}_${TYPE}-backup-${NAME}-$i" --output text)

#wait until the ami has been created so we can get the snapshot id's
echo -n "Waiting for backup (${AMIID}) to complete"

while [  $(aws ec2 describe-images --image-ids $AMIID --output json | grep "\"State\":" | awk -F'"' '{ print $4 }') == "pending" ]
do
  echo -n "."
  sleep 6
done

STATUS=$(aws ec2 describe-images --image-ids $AMIID --output json | grep "\"State\":" | awk -F'"' '{ print $4 }')

case "$STATUS" in
  failed)
    echo "."; echo "Backup: Failed @$(date +%H:%M:%S)"
    RESULT="FAILED"
  ;;
  available)
    echo "."; echo "Backup: Continuing"

    #need to get the snapshot id's so we can tag them
    echo "Getting image snapshots"
    declare -a TOTAG=($(aws ec2 describe-images --image-ids $AMIID --output json | grep "\"SnapshotId\":" | awk -F'"' '{ print $4 }'))
    TOTAG+=("$AMIID")

    echo "Adding tags to images: ${TOTAG[@]}"
    aws ec2 create-tags --resources ${TOTAG[@]} --tags Key=Name,Value="${TYPE} backup" --output text

    declare -a NEWSNAPS=($(aws ec2 describe-images --owners ${OWNERS} --image-ids $AMIID --output text | grep EBS | awk '{ print $4 }'))

    #echo "Tagging snapshots: ${NEWSNAPS[@]}"
    #aws ec2 create-tags --resources ${NEWSNAPS[@]} --tags Key=Description,Value="${TYPE} backup - ${CURDATE}" --output text

    #check for older backups
    #Need to do this because I use sed to purge based on row number this makes it less confising for user.
    ((NUMKEEP++))

    echo "Getting list of images" 
    declare -a CURBACKUPS=($(aws ec2 describe-images --owners ${OWNERS} --filters "Name=description,Values=*${TYPE}-backup-${NAME}-$i*" --query "Images[].[Description ImageId]" --output text | sort -r | sed -n "${NUMKEEP},\$p" | awk '{print $2}'))

    echo "Images: ${CURBACKUPS[@]}"
    #deregister old images
    for id in ${CURBACKUPS[@]}
    do
      #need to get the snapshots before we deregister the ami
      echo "Getting snapshots for image: $id"
      declare -a DELSNAPS=($(aws ec2 describe-images --owners ${OWNERS} --image-ids $id --output text | grep EBS | awk '{ print $4 }'))

      #now we can deregister the ami
      echo "Deregistering AMI: $id"
      DEREGSTAT=$(aws ec2 deregister-image --image-id $id --output text)

      #now we have to delete the snaps
      for ds in ${DELSNAPS[@]}
      do
        echo "Deleting snapshot: $ds"
        DELSNAPSTAT=$(aws ec2 delete-snapshot --output text --snapshot-id $ds)
      done
    done

    echo "Backup: Completed"
    echo ""
    echo "####################################"
    echo ""
  ;;
esac
